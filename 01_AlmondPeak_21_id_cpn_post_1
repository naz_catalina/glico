
%macro purch_id3(pref=,period=,jan=);

proc sql stimer;
	%connect_db(&db, libname=y, dbmstemp=yes);
option compress=yes;
/*****************************/
*BULKLOAD
/*****************************/
*Step 1-1 : Load JAN data;
CREATE TABLE &db..&bulk_pref.jan (bulkload=yes dbtype=(upc_cd='bigint') bl_options="logdir &logdir.") AS
SELECT 
	DISTINCT grp_nbr, upc_cd 

FROM 
	&jan.
;
/****************************************************/
					*Get Active ID;
/****************************************************/
*Step 1-1 : Start filter(first 8 weeks);
EXECUTE
(
    CREATE TEMP TABLE pre_8w AS
	    SELECT
	        l.parent_corp_key,
	        o.pid_key,
	        COUNT(DISTINCT d.cmc_we_dt) AS week_times

	    FROM
	        order_v 				AS o
	        INNER JOIN location_v 	AS l 	ON o.cmc_chn_str_nbr = l.cmc_chn_str_nbr
			INNER JOIN date_v 		AS d 	ON o.cal_dt = d.cal_dt

	    WHERE
	        o.cal_dt BETWEEN &st_dt_P1. AND &ed_dt_P1. 
	        AND l.cmc_chn_nbr IN (&chain_no.)
	        AND o.pid_key > 0
	        AND o.ord_amt > 0
	        AND %common_criteria(o)

	    GROUP BY 
	    	1, 2

	    HAVING
	        week_times >= &freq.

	DISTRIBUTE ON RANDOM
) by &db.
;

*Step 1-2 : Start filter(last 8 weeks);
EXECUTE
(
    CREATE TEMP TABLE post_8w AS
	    SELECT
	        l.parent_corp_key,
	        o.pid_key,
	        COUNT(DISTINCT d.cmc_we_dt) AS week_times

	    FROM
	        order_v 				AS o
	        INNER JOIN location_v 	AS l 	ON o.cmc_chn_str_nbr = l.cmc_chn_str_nbr
			INNER JOIN date_v 		AS d 	ON o.cal_dt = d.cal_dt

	    WHERE
	        o.cal_dt BETWEEN &st_dt_P2. AND &ed_dt_P2. 
	        AND l.cmc_chn_nbr IN (&chain_no.)
	        AND o.pid_key > 0
	        AND o.ord_amt > 0
	        AND %common_criteria(o)

	    GROUP BY 
	    	1, 2

	    HAVING
	        week_times >= &freq.

	DISTRIBUTE ON RANDOM
) by &db.
;

*Step 1-3 Active id filter(id in both first and last 8 weeks);
EXECUTE
(
    CREATE TEMP TABLE act_id_flt AS
    SELECT
        a.parent_corp_key,
        a.pid_key

    FROM
        pre_8w 				AS a
        INNER JOIN post_8w 	AS b 	ON b.parent_corp_key = a.parent_corp_key AND b.pid_key = a.pid_key

	DISTRIBUTE ON RANDOM
) by &db.
;

CREATE TABLE work.act_id_flt AS
SELECT * 
FROM &db..act_id_flt 
;

		*Step 1-3-1 : Active id COUNT(total active id);
		EXECUTE
		(
		    CREATE TEMP TABLE act_id_cnt AS
		        SELECT
		            COUNT(DISTINCT parent_corp_key||'_'||pid_key) as id_cnt

		        FROM
		            act_id_flt

		    DISTRIBUTE ON RANDOM
		) by &db.
		;

		CREATE TABLE work.act_id_cnt AS
		SELECT * 
		FROM &db..act_id_cnt 
		;
/****************************************************/
				*Get Purchase Data;
/****************************************************/
*Step 3 : Get purchase data(by category);
EXECUTE 
(
	CREATE TEMP TABLE purch_id AS
		SELECT
			d.cmc_we_dt,
			o.pid_key,
			l.parent_corp_key,
			j.grp_nbr,
			SUM(o.purch_amt) 			AS amt,
			SUM(o.purch_qty)			AS qty,
			COUNT(DISTINCT o.tran_nbr)	AS tran

		FROM  
			order_upc_v 				AS o
		    INNER JOIN location_v 		AS l 	ON o.cmc_chn_str_nbr = l.cmc_chn_str_nbr
		    INNER JOIN act_id_flt		AS i 	ON o.pid_key = i.pid_key 	AND l.parent_corp_key = i.parent_corp_key
		    INNER JOIN &bulk_pref.jan 	AS j 	ON o.upc_cd = j.upc_cd
			INNER JOIN date_v			AS d 	ON o.cal_dt = d.cal_dt

		WHERE
		    o.cal_dt BETWEEN &&st_dt_P&period. AND &&ed_dt_P&period. 
			AND o.pid_key > 0
			AND o.purch_amt > 0
			AND o.purch_qty > 0
		    AND o.cmc_chn_nbr IN (&chain_no.)
			AND %common_criteria(o)	

		GROUP BY 
			1, 2, 3, 4

		ORDER BY
			1 ASC

	DISTRIBUTE ON RANDOM 
) BY &db
;

		CREATE TABLE work.purch_id_&pref._P&period. AS
		SELECT*
		FROM &db..purch_id
		;

		EXECUTE
		(
			CREATE TEMP TABLE purch_id_cnt AS			
				SELECT
		            COUNT(DISTINCT parent_corp_key||'_'||pid_key) 	AS id_cnt,
					SUM(amt) 										AS amt ,
					SUM(qty) 										AS qty ,
					SUM(tran) 										AS tran
				
				FROM	
					purch_id

			DISTRIBUTE ON RANDOM
		) by &db.			
		;

		CREATE TABLE work.purch_id_cnt_&pref._P&period. AS
		SELECT*
		FROM &db..purch_id_cnt 
		;

quit;
%mend;

%purch_id3(pref=cpn_glico,  jan=jan_gli,      period=5);  
%purch_id3(pref=pos_glico,  jan=jan_gli,      period=6);  
%purch_id3(pref=cpn_meiji,  jan=jan_mei,      period=5);  
%purch_id3(pref=pos_meiji,  jan=jan_mei,      period=6);  
%purch_id3(pref=cpn_lotte,  jan=jan_lot,      period=5);  
%purch_id3(pref=pos_lotte,  jan=jan_lot,      period=6);  
